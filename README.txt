ELEMENTS NUMBERFIELD
--------------------

IGNACIO SEGURA (https://www.drupal.org/u/nachenko)
DANIEL IREGUI (https://www.drupal.org/u/danielomaza)


Elements numberfield extends Elements module (https://www.drupal.org/project/elements).

By using Elements, you can use HTML5 elements on your forms, but in order to do that, you need to write your own custom code to build or alter your form. Our module removes the need to write custom code for Fields, as it provides an HTML5 number widget for all Integer and Decimal fields. Just select “HTML5 numberfield” as controller widget in those fields you want to use it.

It also provides support for minimum, maximum and step attributes.